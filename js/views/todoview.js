
<script type="text/template" id="item-template">
  <div class="view">
    <input class="toggle" type="checkbox"></input>
    <label><%- title %></label>
  </div>
</script>


// renders individual todo items list (li)
app.TodoView = Backbone.View.extend({
    tagName: 'li',
    template: _.template($('#item-template').html()),
    render: function(){
      this.$el.html(this.template(this.model.toJSON()));
      return this; // enable chained calls
    }
  });
  
  