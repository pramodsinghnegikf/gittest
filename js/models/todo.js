var app = {}; // create namespace for our app

app.Todo = Backbone.Model.extend({
  initialize: function()
  {
      console.log("a new todo list is created");
  },
  defaults: {
    title: '',
    completed: false
  }
});